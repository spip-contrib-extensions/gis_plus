# "gis_plus" GIS Surcharge / markers et legend 
permet d'attribuer des icones aux points GIS liés à des articles et d'en afficher la légende

## Documentation

Disponible sur [SPIP Contrib](https://contrib.spip.net/5481)


## Comment ça fonctionne ?
- Le plugin lors de son installation créé le groupe de mot-clef "_marker_icon"
ainsi que cinq mots-clefs enfants et leur logo/icône de départ extraits du dossier d'images.

On peut voir en ligne le résultat (https://toten-occitanie.fr/retours_experiences/) (bas de page)