<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function ajouter_logos_mots() {
	
	// retrouver le groupe
	$id_groupe = sql_getfetsel('id_groupe','spip_groupes_mots',"titre LIKE '%_marker_icon%'");
	
	spip_log("N° groupe des icones legende = $id_groupe",'gis_plus' );
	
	// Pour ajouter les logos
	//1 lister les titres des mots créés
	//2 utiliser le fichier image ayant un titre homothétique
	//3 lier l'image en tant que logo du mot et avec le role "logo" si champ role existe
	include_spip('action/editer_logo');

	$result = sql_select('id_mot,titre', "spip_mots", "id_groupe=$id_groupe");

	while ($row = sql_fetch($result)){
		$objet = 'mot';
		$id_objet = $row['id_mot'];
		$etat ='on';
		$titre = $row['titre'];
		$titre_img = strtolower(str_replace(' ','_',$titre));

		$source = find_in_path(_DIR_PLUGIN_GIS_PLUS.'images/'.$titre_img.'.png');

		spip_log("Ajout du logo $titre_img avec $source",'gis_plus' );
		logo_modifier($objet, $id_objet, $etat, $source);

	}
	
}