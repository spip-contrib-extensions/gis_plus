<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function peupler_base_gis_plus() {
			
	//retrouver le groupe créé
	$id_groupe = sql_getfetsel('id_groupe','spip_groupes_mots',"titre LIKE '%_marker_icon%'");
	
	if ($id_groupe > 0){
		spip_log("groupe _marker_icon id $id_groupe existe deja","peupler_base_gis_plus");
		return;
	}
	
	$id_groupe = sql_insertq('spip_groupes_mots', array(
		'titre' => '_marker_icon',
		'obligatoire'  => 'non',
		'descriptif'  => stripcslashes("Ce groupe de mots-clés techniques permet de lier des articles géolocalisés à un de ses mot-clefs. \n\n
		Le logo d'un mot-clef de ce groupe sera affiché comme marker du point GIS lié, son titre s'affichera dans la légende de la carte avec son logo. \n\n
		<span style='color:red'>Attention ! </span>Ne pas modifier le titre du groupe '_marker_icon', il faut aussi garder le titre du mot-clef '_marker_defaut=' et ajouter votre titre à la suite. \n\n
		Sauf '_marker_defaut=' il est possible de supprimer ou d'ajouter d'autres mots-clefs et de modifier tous les titres et les logos.
		"),
		'tables_liees' => 'articles',
		'technique' => 'oui',
		'minirezo' => 'oui',
		'comite' => 'oui',
		'forum' => 'non',
	));

	//si plugin mots_arborescents sur groupe actif
	if(test_plugin_actif('motsar') !== false){
		sql_insertq('spip_groupes_mots', array('id_groupe_racine' => $id_groupe),'id_groupe='.intval($id_groupe));
	}
	
	if ($id_groupe > 0) {
		
		sql_insertq_multi('spip_mots', array(
			array(
				'titre' => '_marker_defaut=',
				'id_groupe' => $id_groupe,
				'type' => '_marker_icon',
			),
			array(
				'titre' => 'Marker rouge',
				'id_groupe' => $id_groupe,
				'type' => '_marker_icon',
			),
			array(
				'titre' => 'Marker vert',
				'id_groupe' => $id_groupe,
				'type' => '_marker_icon',
			),
			array(
				'titre' => 'Marker bleu',
				'id_groupe' => $id_groupe,
				'type' => '_marker_icon',
			),
			array(
				'titre' => 'Marker orange',
				'id_groupe' => $id_groupe,
				'type' => '_marker_icon',
			),
		));
		
	}
}