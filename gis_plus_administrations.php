<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin gis_plus
 *
 * @plugin     gis_plus
 * @copyright  2021
 * @author     Anne-lise Martenot
 * @licence    GPL v3
 * @package    SPIP\gis_plus\Installation
 */
 
if (!defined("_ECRIRE_INC_VERSION")){
	return;
}

include_spip('base/gis_plus_peupler_base');
include_spip('base/gis_plus_ajouter_logos');


function gis_plus_upgrade($nom_meta_base_version, $version_cible){

	$maj = array();
	
	$maj['create'] = array(
		array('peupler_base_gis_plus'),
		array('ajouter_logos_mots')
	);
				
	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
* Fonction de désinstallation du plugin.
**/
function gis_plus_vider_tables($nom_meta_base_version) {

	// suppression des liaisons des documents liés aux mots du groupe
	include_spip('action/dissocier_document');

	$documents_marker = sql_select(
		array(
			"docs.id_document AS id_doc",
			"docs.fichier AS fichier",
			"docs.titre AS titre",
			"M.id_mot AS id_mot",
			"M.type AS type_mot"),
		array(
			"spip_documents AS docs", 
			"spip_documents_liens AS lien", 
			"spip_mots AS M"),
		array(
			"docs.id_document = lien.id_document", 
			"M.id_mot = lien.id_objet", 
			"M.type LIKE '%_marker_icon%'", 
			"lien.objet='mot'"
			),
		 );
	while ($row=sql_fetch($documents_marker)) {

		$id_document = $row['id_doc'];
		$objet = 'mot';
		$id_objet =  $row['id_mot'];
		
		supprimer_lien_document($id_document, $objet, $id_objet);
		spip_log("suppression liaison doc N°$id_document / mot N°$id_objet",'gis_plus');
	}

	// suppression des mots du groupe
	sql_delete('spip_groupes_mots', "titre LIKE '%_marker_icon%'");
	sql_delete('spip_mots', "type LIKE '%_marker_icon%'");

	effacer_meta($nom_meta_base_version);
}