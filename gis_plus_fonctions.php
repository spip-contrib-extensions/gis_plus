<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function pas_retour_ligne($texte){
	$texte = preg_replace('/[\r\n]+/', '', $texte);
	return $texte;
}