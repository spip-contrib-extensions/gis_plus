<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

//Ajoute la css de la légende et des icônes des points GIS
function gis_plus_insert_head_css($flux) {
	$css = generer_url_public('gis_plus.css');
	$flux .= "<link rel='stylesheet' type='text/css' media='all' href='$css' />\n";
	return $flux;
}

//Ajoute le js qui inclut la légende
function gis_plus_insert_head($flux) {
	$js = generer_url_public('gis_plus.js');
	$flux .= "<script src='$js'></script>\n";
	return $flux;
}