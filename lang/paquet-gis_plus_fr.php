<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'perso_infolettres_description' => 'GIS surcharge pour la cartographie, markers et legend',
	'perso_infolettres_nom' => 'GIS+ surcharge pour la cartographie',
	'perso_infolettres_slogan' => 'GIS surcharge pour la cartographie',
);
